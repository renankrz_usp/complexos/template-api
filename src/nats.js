const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const subscription = conn.subscribe('users', opts);

    subscription.on('message', async (msg) => {
      const message = JSON.parse(msg.getData());
      const database = mongoClient.db('eventStore');
      const collection = database.collection('users');

      await collection.insertOne(message);
    });
  });

  return conn;
};
