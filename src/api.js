const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => {
    res.json("Hello, World!");
  });

  api.post("/users", async (req, res) => {
    // verify if all fields are present
    const missingField = checkFieldsExistence(req.body);

    if (missingField != undefined) {
      res.status(400).json({
        "error": `Request body had missing field ${missingField}`,
      });
      return;
    }

    // verify if the fields match the specified domain
    const nonConformingField = await checkFieldsConformity(req.body, mongoClient);

    if (nonConformingField != undefined) {
      res.status(400).json({
        "error": `Request body had malformed field ${nonConformingField}`,
      });
      return;
    }

    // verify if passwordConfirmation matches password
    const { password, passwordConfirmation } = req.body;

    if (password != passwordConfirmation) {
      res.status(422).json({
        "error": "Password confirmation did not match",
      });
      return;
    }

    // publish event
    const id = uuid();
    const event = {
      "eventType": "UserCreated",
      "entityId": id,
      "entityAggregate": {
        "name": req.body.name,
        "email": req.body.email,
        "password": req.body.password,
      }
    }

    stanConn.publish('users', JSON.stringify(event));

    // respond
    const userRes = {
      user: {
        "id": id,
        "name": req.body.name,
        "email": req.body.email,
      }
    }

    res.status(201).json(userRes);
  });

  api.delete("/users/:uuid", async (req, res) => {
    // verify if auth header is present
    const authHeader = req.header('Authentication');

    if (!authHeader) {
      res.status(401).json({
        'error': 'Access Token not found',
      });
      return;
    }

    // verify if uuids match
    const id = req.params.uuid;
    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, secret);

    if (decoded.id != id) {
      res.status(403).json({
        'error': 'Access Token did not match User ID',
      });
      return;
    }

    // publish event
    const event = {
      "eventType": "UserDeleted",
      "entityId": id,
      "entityAggregate": {},
    }

    stanConn.publish('users', JSON.stringify(event));

    // respond
    res.status(200).json({"id": id});
  });

  return api;
};

const checkFieldsExistence = (body) => {
  const { name, email, password, passwordConfirmation } = body;

  if (name == undefined) {
    return "name";
  }

  if (email == undefined) {
    return "email";
  }

  if (password == undefined) {
    return "password";
  }

  if (passwordConfirmation == undefined) {
    return "passwordConfirmation";
  }
};

const checkFieldsConformity = async (body, mongoClient) => {
  const { name, email, password } = body;

  if (typeof name != 'string'
    || name.length == 0) {
    return "name";
  }

  const db = mongoClient.db('viewDB');
  const collection = db.collection('user');
  const existingUser = await collection.findOne({ email });

  if (existingUser
    || typeof email != 'string'
    || !(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))) {
    return "email";
  }

  if (typeof password != 'string'
    || password.length < 8
    || password.length > 32) {
    return "password";
  }
}
